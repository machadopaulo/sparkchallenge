package utils;

import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class DataReader {


    public static String readSqlToString(String filePath) {
        try {
            return FileUtils.readFileToString(new File("src/main/resources/" + filePath), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void loadData(SparkSession spark) {
        DataReader.loadWhatsonData(spark);
        DataReader.loadStartedStreams(spark);
    }

    public static void loadStartedStreams(SparkSession spark) {
        StructType startedStreams = new StructType()
                .add("dt", DataTypes.DateType)
                .add("time", DataTypes.StringType)
                .add("device_name", DataTypes.StringType)
                .add("house_number", DataTypes.LongType)
                .add("user_id", DataTypes.StringType)
                .add("country_code", DataTypes.StringType)
                .add("program_title", DataTypes.StringType)
                .add("season", DataTypes.StringType)
                .add("season_episode", DataTypes.StringType)
                .add("genre", DataTypes.StringType)
                .add("product_type", DataTypes.StringType);

        Dataset<Row> dfStartedStreams = spark.read()
                .option("mode", "DROPMALFORMED")
                .option("header", "true")
                .schema(startedStreams)
                .csv("src/main/resources/input_data/started_streams_new.csv");

        dfStartedStreams.createOrReplaceTempView("startedStreams");
    }

    public static void loadWhatsonData(SparkSession spark) {
        StructType whatson = new StructType()
                .add("dt", DataTypes.DateType)
                .add("house_number", DataTypes.LongType)
                .add("title", DataTypes.StringType)
                .add("product_category", DataTypes.StringType)
                .add("broadcast_right_region", DataTypes.StringType)
                .add("broadcast_right_vod_type", DataTypes.StringType)
                .add("broadcast_right_start_date", DataTypes.StringType)
                .add("broadcast_right_end_date", DataTypes.StringType);

        Dataset<Row> dfWhatson = spark.read()
                .option("mode", "DROPMALFORMED")
                .option("header", "true")
                .schema(whatson)
                .csv("src/main/resources/input_data/whatson.csv");

        dfWhatson.createOrReplaceTempView("whatson");

        DataReader.createCountryCodeWhatson(spark);
    }

    private static void createCountryCodeWhatson(SparkSession spark) {
        Dataset<Row> dfWhatson;
        dfWhatson = spark.sql(DataReader.readSqlToString("sales_rentals_broad_cast_rights_task/create_country_code.sql"));
        dfWhatson.createOrReplaceTempView("whatson");
    }
}
