import org.apache.spark.sql.SparkSession;
import tasks.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Task selectedTask = null;
        while (selectedTask == null) {
            System.out.println("Enter the number of the task you want to execute:");
            System.out.println("1 - Sales and rentals broadcast rights");
            System.out.println("2 - Product and user count");
            System.out.println("3 - Genre and time of day (Only top genre per hour)");
            System.out.println("4 - Genre and time of day (Full result)");

            String taskNumber = scanner.nextLine();

            switch (taskNumber) {
                case "1":
                    selectedTask = new SalesRentalsBroadCastRightsTask();
                    break;
                case "2":
                    selectedTask = new ProductUserCountTask();
                    break;
                case "3":
                    selectedTask = new GenreTimeTaskOnlyTopPerHour();
                    break;
                case "4":
                    selectedTask = new GenreTimeTaskFull();
                    break;
                default:
                    System.out.println("Invalid Task");
                    break;
            }
        }

        SparkSession spark = SparkSession
                .builder()
                .appName("Spark Java Challenge")
                .master("local[*]")
                .getOrCreate();

        selectedTask.run(spark);
    }

}
