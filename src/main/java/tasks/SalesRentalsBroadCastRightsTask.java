package tasks;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import utils.DataReader;

public class SalesRentalsBroadCastRightsTask implements Task {

    @Override
    public void readData(SparkSession spark) {
        DataReader.loadData(spark);
    }

    @Override
    public void run(SparkSession spark) {
        this.readData(spark);

        Dataset<Row> whatsonClusteredBy = spark.sql(DataReader.readSqlToString("sales_rentals_broad_cast_rights_task/cluster_whatson_by_country_house.sql"));
        whatsonClusteredBy.createOrReplaceTempView("whatson");

        Dataset<Row> startedStreamCluteredBy = spark.sql(DataReader.readSqlToString("sales_rentals_broad_cast_rights_task/cluster_streams_by_coutry_house.sql"));
        startedStreamCluteredBy.createOrReplaceTempView("startedStreams");

        Dataset<Row> result = spark.sql(DataReader.readSqlToString("sales_rentals_broad_cast_rights_task/join_whatson_streams.sql"));

        String resultPath = "sales_rentals_broad_cast_rights_task";
        Task.saveResult(result, resultPath);
    }
}