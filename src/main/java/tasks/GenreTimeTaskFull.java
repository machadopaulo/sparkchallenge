package tasks;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import utils.DataReader;

public class GenreTimeTaskFull implements tasks.Task {

    @Override
    public void readData(SparkSession spark) {
        DataReader.loadStartedStreams(spark);
    }

    @Override
    public void run(SparkSession spark) {
        this.readData(spark);
        Dataset<Row> result = spark.sql(DataReader.readSqlToString("genre_time_task/group_streams.sql"));
        
        String resultPath = "genre_time_task_full";
        tasks.Task.saveResult(result, resultPath);
    }

}