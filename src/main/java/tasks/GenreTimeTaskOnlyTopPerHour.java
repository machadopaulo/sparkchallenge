package tasks;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import utils.DataReader;

public class GenreTimeTaskOnlyTopPerHour implements tasks.Task {

    @Override
    public void readData(SparkSession spark) {
        DataReader.loadStartedStreams(spark);
    }

    @Override
    public void run(SparkSession spark) {
        this.readData(spark);
        Dataset<Row> allResults = spark.sql(DataReader.readSqlToString("genre_time_task/group_streams_with_count.sql"));
        allResults.createOrReplaceTempView("resultGenreTime");

        Dataset<Row> finalResult = spark.sql(DataReader.readSqlToString("genre_time_task/only_top_per_hour.sql"));

        String resultPath = "genre_time_task_only_top_per_hour";
        tasks.Task.saveResult(finalResult, resultPath);
    }

}