package tasks;

import org.apache.commons.io.FileUtils;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.File;
import java.io.IOException;

public interface Task {

    public void run(SparkSession spark);

    public void readData(SparkSession spark);

    public static void saveResult(Dataset<Row> result, String resultPath) {
        resultPath = "./spark-outputs/" + resultPath;
        Task.removeOldResults(resultPath);
        result.repartition(1)
                .write()
                .option("header", "true")
                .csv(resultPath);
    }

    public static void removeOldResults(String resultPath) {
        try {
            FileUtils.deleteDirectory(new File(resultPath));
        } catch (IOException e) {
            return;
        }

    }
}
