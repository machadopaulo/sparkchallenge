package tasks;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import utils.DataReader;


public class ProductUserCountTask implements Task {

    @Override
    public void readData(SparkSession spark) {
        DataReader.loadStartedStreams(spark);
    }

    @Override
    public void run(SparkSession spark) {
        this.readData(spark);
        Dataset<Row> result = spark.sql(DataReader.readSqlToString("product_user_count_task/group_streams.sql"));
        String resultPath = "product_user_count_task";
        Task.saveResult(result, resultPath);
    }

}