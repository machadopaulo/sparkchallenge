SELECT max(dt) as dt,
       program_title,
       device_name,
       country_code,
       product_type,
       cast(collect_set(user_id) as string) AS unique_users,
       count(*) AS content_count
FROM startedStreams
GROUP BY program_title,
         device_name,
         country_code,
         product_type