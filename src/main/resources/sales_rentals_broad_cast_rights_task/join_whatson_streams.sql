SELECT whatson.dt,
        time,
        device_name,
        whatson.house_number,
        user_id,
        whatson.country_code,
        program_title,
        season,
        season_episode,
        genre,
        product_type,
        whatson.broadcast_right_start_date,
        whatson.broadcast_right_end_date
FROM whatson whatson
INNER JOIN startedStreams startedStreams ON (startedStreams.country_code = whatson.country_code
                                             AND startedStreams.house_number = whatson.house_number)