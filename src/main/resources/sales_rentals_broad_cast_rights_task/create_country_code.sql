SELECT *,
-- change to java method to be more generic
       CASE
           WHEN broadcast_right_region = 'Sweden' THEN 'se'
           WHEN broadcast_right_region = 'Norway' THEN 'no'
           WHEN broadcast_right_region = 'Denmark' THEN 'dk'
           WHEN broadcast_right_region = 'Finland' THEN 'fi'
           ELSE 'unkown'
           END AS country_code
FROM whatson