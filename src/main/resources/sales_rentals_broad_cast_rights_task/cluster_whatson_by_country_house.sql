SELECT dt,
       house_number,
       title,
       product_category,
       broadcast_right_region,
       broadcast_right_vod_type,
       broadcast_right_start_date,
       broadcast_right_end_date,
       country_code
FROM whatson
WHERE broadcast_right_vod_type in ('TVOD', 'EST')
AND dt = (SELECT max(dt) FROM whatson WHERE broadcast_right_vod_type in ('TVOD', 'EST'))
CLUSTER BY country_code,
         house_number