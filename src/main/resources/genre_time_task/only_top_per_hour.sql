SELECT watched_hour,
       genre,
       unique_users
FROM
    (SELECT *,
            row_number() OVER (PARTITION BY watched_hour
                ORDER BY count_users DESC) AS row_number
     FROM resultGenreTime) tmp
WHERE row_number = 1