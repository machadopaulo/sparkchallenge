SELECT split(time, ':')[0] as watched_hour,
       genre,
       cast(collect_set(user_id) as string) AS unique_users
FROM startedStreams
GROUP BY genre, watched_hour
ORDER BY watched_hour DESC,  count(distinct user_id) DESC