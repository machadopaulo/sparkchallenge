SELECT split(time, ':')[0] as watched_hour,
       genre,
       cast(collect_set(user_id) as string) AS unique_users,
       count(distinct user_id) AS count_users
FROM startedStreams
GROUP BY genre, watched_hour