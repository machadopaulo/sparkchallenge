.PHONY: build run test

all: help

help:
	@echo "SparkJavaChallenge Makefile commands description"
	@echo ""
	@echo "install                      Execute maven install"
	@echo "run                          Run project using maven"

install:
	@mvn clean install

run:
	@mvn exec:java -D exec.mainClass=Main -Dexec.cleanupDaemonThreads=false