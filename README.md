# SparkChallenge

### Recommended Settings:
Maven 3.6.3 

Java version: 1.8.0_232

### Project Structure:
    ├── src/main     
    │   ├── java/                                              # Source Code
    │   │   └── tasks/                                         # Tasks implementations
    │   │   └── Main                                           # Main class to launch tasks
    │   └── resources/        
    │       ├──input_data/                                     # Input data to use on tasks
    │       ├── genre_time_task                                # Sqls queries to use on GenreTimeTaskFull and GenreTimeTaskOnlyTopPerHourTask 
    │       ├── product_user_count_task                        # Sqls queries to use on ProductUserCountTask 
    │       └── sale_rentals_broad_cast_rights_task            # Sqls queries to use on SalesRentalsBroadCastRightsTask
    ├── spark-outputs                                          # Spark output files
    ├── ...                                          
    └── README.md
    
### Setting up project:
    make install

### Inputs:
The inputs for tasks are located at `src/main/resources/input_data/`. To run all tasks successfully, two csv files are needed: `whatson.csv` and `started_streams_new.csv`.

`whatson.csv` file should follow this format:
![alt text](docs/whatson.png "whatson")

`started_streams_new.csv` file should follow this format:
![alt text](docs/started_streams.png "started_streams")


### Available Tasks:

#### 1 - Sales and rentals broadcast rights
#### Inputs: `whatson.csv` and `started_streams_new.csv`
#### Output: `spark-outputs/sales_rentals_broad_cast_rights_task/`

The object of this task is to find the broadcast rights for a title on the most recent date of whatson data. This task is only for product types TVOD and EST.

This task process the inputs in this order:

1- Create column country_code at whatson data (Works only with Sweden, Norway, Denmark and Finland, needs an improvement)

2- Cluster whatson data for product types TVOD and EST by the country_code and house_number

3- Cluster started streams data for the max date of whatson data for product types TVOD and EST by the country_code and house_number

4- Join whatson clustered data and started streams clustered data by country_code and house_number

The result has the following data:

* dt
* time
* device_name
* house_number
* user_id 
* country_code
* program_title
* season
* season_episode
* genre
* product_type
* broadcast_right_start_date
* broadcast_right_end_date


#### 2 - Product and user count
#### Input: `src/main/resources/input_datastarted_streams_new.csv`
#### Output: `spark-outputs/product_user_count_task/`

The object of this task is to know how many watches a product is getting and how many unique users are watching the content, in what device, country and what product_type.

This task process the input grouping the data by program_title, device_name, country_code, product_type and getting as result the values:

* program_title

* device_name

* country_code

* product_type

* dt: aggregation max

* unique_users: aggregation unique users as a string list

* content_count: aggregation count


#### 3 - Genre and time of day (Only top genre per hour)
#### Input: `started_streams_new.csv`
#### Output: `spark-outputs/genre_time_task_only_top_per_hour/`


The object of this task is to list the genre with the most unique users for each hour of the day.

This task process the input in this order:

1- Grouping the input by genre and watched_hour and getting as result the values: watched_hour, genre, unique_users (aggregation unique users as a string list), count_users (number of unique users)

2- For each watched_hour, get only the genre with the most number of of unique users
 
The result has the following data:

* watched_hour
* genre
* unique_users: aggregation unique users as a string list

#### 4 - Genre and time of day (Full result)
#### Input: `started_streams_new.csv`
#### Output: `spark-outputs/genre_time_task_full/`

The object of this task is to list what time and which are the users watching a genre.

This task process the input grouping by genre and watched_hour and getting as result the values:

* watched_hour
* genre
* unique_users: aggregation unique users as a string list


### Running application:
    make run

### Running tests:
//TODO

